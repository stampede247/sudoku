﻿using QuickFont;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;

namespace Sudoku
{
	class GameBoard
	{
		public static int WIDTH = 9, HEIGHT = 9;
		public static int spacing = 6, lineThickness1 = 1, lineThickness2 = 4;
		public float tileSize;

		public QFont font, fontSmall;
		public Texture2D dot, background;
		public RectangleF gameRec;
		public Point mouseHover, mouseInsideHover;
		public bool[] completed;

		public GridSpace[,] grid;
		public EndGameHandler endGame;

		public Rectangle GetRec(int x, int y)
		{
			return new Rectangle((int)(gameRec.Left + spacing + x * (tileSize + spacing)),
				(int)(gameRec.Top + spacing + y * (tileSize + spacing)),
				(int)tileSize, (int)tileSize);
		}
		public Block GetBlock(int blockX, int blockY)
		{
			return new Block(blockX, blockX, this);
		}

		public GameBoard(Rectangle boardRec)
		{
			font = new QFont("Content/Fonts/Default.ttf", 40);
			fontSmall = new QFont("Content/Fonts/Default.ttf", 12);
			background = TextureHandler.GetTexture("paperBack.jpg");

			#region Create Dot
			{
				dot = new Texture2D("dot", GL.GenTexture(), 1, 1);
				GL.BindTexture(TextureTarget.Texture2D, dot.Id);
				Bitmap bitmap = new Bitmap(1, 1);
				bitmap.SetPixel(0, 0, Color.White);
				BitmapData data = bitmap.LockBits(new Rectangle(0, 0, 1, 1),
					ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
					data.Width,
					data.Height,
					0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
					PixelType.UnsignedByte, data.Scan0);

				bitmap.UnlockBits(data);

				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Nearest);
			}
			#endregion

			this.gameRec = boardRec;
			this.tileSize = (boardRec.Width - spacing) / 9f - spacing;

			#region Initiate Grid
			{
				grid = new GridSpace[WIDTH, HEIGHT];
				for (int x = 0; x < WIDTH; x++)
				{
					for (int y = 0; y < HEIGHT; y++)
					{
						grid[x, y] = new GridSpace(x, y);
					}
				}
			}
			#endregion

			CreatePuzzle(true);
			endGame = new EndGameHandler(this);
		}

		private void ClearGrid()
		{
			for (int x = 0; x < WIDTH; x++)
			{
				for (int y = 0; y < HEIGHT; y++)
				{
					grid[x, y].filled = false;
					grid[x, y].permanant = false;
				}
			}
		}
		private bool GenerateGrid()
		{
			ClearGrid();
			for (int x = 0; x < WIDTH; x++)
			{
				for (int y = 0; y < HEIGHT; y++)
				{
					List<int> possible = PossibleValuesFor(x, y);
					if (possible.Count == 0)
					{
						Console.WriteLine("Found impossible at ({0},{1})", x, y);
						return false;
					}

					int i = My.rand.Next(0, possible.Count);
					grid[x, y].filled = true;
					grid[x, y].permanant = true;
					grid[x, y].number = possible[i];

					while (FillNeeded(true)) { }
				}
			}

			return true;
		}
		private void ClearSpaces(bool easy = false)
		{
			if (easy)
				FindNecassarySpace();
			else 
				while (FindNecassarySpace()) { }
		}
		private bool FindNecassarySpace()
		{
			for (int i = 0; i < 100; i++)
			{
				int x = 0;
				int y = 0;
				do
				{
					x = My.rand.Next(0, 9);
					y = My.rand.Next(0, 9);
				} while (!grid[x, y].filled);

				List<int> possible = PossibleValuesFor(x, y);
				if (possible.Count == 1)
				{
					grid[x, y].filled = false;
					grid[x, y].permanant = false;
					return true;
				}
			}

			return false;
		}
		public void CreatePuzzle(bool easy = false)
		{
			while (!GenerateGrid()) { };

			ClearSpaces(easy);

			UpdateCompletedNums();
		}

		public void Update(double time)
		{
			if (!endGame.running)
			{
				mouseInsideHover = new Point(-1, -1);
				mouseHover = new Point(-1, -1);
				#region Mouse Hovering
				for (int x = 0; x < WIDTH; x++)
				{
					for (int y = 0; y < HEIGHT; y++)
					{
						Rectangle rec = GetRec(x, y);
						if (rec.Contains(Program.window.Mouse.X, Program.window.Mouse.Y))
						{
							mouseHover = new Point(x, y);
							mouseInsideHover.X = (int)((Program.window.Mouse.X - rec.X) / (rec.Width / 3f));
							mouseInsideHover.Y = (int)((Program.window.Mouse.Y - rec.Y) / (rec.Height / 3f));
						}
					}
				}
				#endregion

				#region Clicking
				if (My.MousePress(true))
				{
					if (mouseHover != new Point(-1, -1) && mouseInsideHover != new Point(-1, -1))
					{
						if (!grid[mouseHover.X, mouseHover.Y].filled)
						{
							int newNumber = mouseInsideHover.X + mouseInsideHover.Y * 3 + 1;
							grid[mouseHover.X, mouseHover.Y].filled = true;
							grid[mouseHover.X, mouseHover.Y].number = newNumber;
							UpdateCompletedNums();
							CheckWin();
						}
					}
				}
				if (My.MousePress(false))
				{
					if (mouseHover != new Point(-1, -1))
					{
						if (!grid[mouseHover.X, mouseHover.Y].permanant && grid[mouseHover.X, mouseHover.Y].filled)
						{
							grid[mouseHover.X, mouseHover.Y].filled = false;
							UpdateCompletedNums();
							CheckWin();
						}
					}
				}
				#endregion
			}

			if (My.KeyPress(OpenTK.Input.Key.A))
			{
				if (endGame.running)
				{
					endGame.Reset();
					this.CreatePuzzle();
				}
				else
					endGame.Create();
			}

			endGame.Update(time);
		}
		private void UpdateCompletedNums()
		{
			completed = new bool[10];
			for (int i = 1; i <= 9; i++)
			{
				int num = 0;
				for (int x = 0; x < WIDTH; x++)
				{
					for (int y = 0; y < HEIGHT; y++)
					{
						if (grid[x, y].filled && grid[x, y].number == i)
							num++;
					}
				}

				completed[i] = (num >= 9);
			}
		}
		private void CheckWin()
		{
			for (int x = 0; x < WIDTH; x++)
			{
				for (int y = 0; y < HEIGHT; y++)
				{
					if (!grid[x, y].filled && PossibleValuesFor(x, y).Contains(grid[x,y].number))
					{
						return;
					}
				}
			}

			endGame.Create();
		}

		public void Draw()
		{
			GL.BindTexture(TextureTarget.Texture2D, background.Id);
			#region Grid Background
			GL.Begin(PrimitiveType.Quads);
			GL.Color3(Color.Black);
			GL.TexCoord2(0, 0); GL.Vertex2(gameRec.Left, gameRec.Top);
			GL.TexCoord2(1, 0); GL.Vertex2(gameRec.Right, gameRec.Top);
			GL.TexCoord2(1, 1); GL.Vertex2(gameRec.Right, gameRec.Bottom);
			GL.TexCoord2(0, 1); GL.Vertex2(gameRec.Left, gameRec.Bottom);
			GL.Color3(Color.LightBlue);
			GL.TexCoord2(0, 0); GL.Vertex2(gameRec.Left + lineThickness2, gameRec.Top + lineThickness2);
			GL.TexCoord2(1, 0); GL.Vertex2(gameRec.Right - lineThickness2, gameRec.Top + lineThickness2);
			GL.TexCoord2(1, 1); GL.Vertex2(gameRec.Right - lineThickness2, gameRec.Bottom - lineThickness2);
			GL.TexCoord2(0, 1); GL.Vertex2(gameRec.Left + lineThickness2, gameRec.Bottom - lineThickness2);
			GL.End();
			#endregion

			if (!endGame.running)
			{
				GL.BindTexture(TextureTarget.Texture2D, dot.Id);
				GL.Begin(PrimitiveType.Quads);

				#region Lines
				GL.Color3(Color.Black);
				GL.TexCoord2(0, 0);
				for (int x = -1; x <= WIDTH - 1; x++)
				{
					bool isThick = ((x + 1) % 3 == 0);
					float width = (isThick ? lineThickness2 : lineThickness1);
					float pos = GetRec(x, 0).Right + spacing / 2f - width / 2f;
					GL.Vertex2(pos + width, gameRec.Top);
					GL.Vertex2(pos + width, gameRec.Bottom);
					GL.Vertex2(pos, gameRec.Bottom);
					GL.Vertex2(pos, gameRec.Top);
				}
				for (int y = -1; y <= HEIGHT - 1; y++)
				{
					bool isThick = ((y + 1) % 3 == 0);
					float width = (isThick ? lineThickness2 : lineThickness1);
					float pos = GetRec(0, y).Bottom + spacing / 2f - width / 2f;
					GL.Vertex2(gameRec.Left, pos);
					GL.Vertex2(gameRec.Right, pos);
					GL.Vertex2(gameRec.Right, pos + width);
					GL.Vertex2(gameRec.Left, pos + width);
				}
				#endregion

				GL.End();

				for (int x = 0; x < WIDTH; x++)
				{
					for (int y = 0; y < HEIGHT; y++)
					{
						grid[x, y].Draw(this, GetRec(x, y),
							new Point(x, y) == mouseHover,
							(IsSameLine(new Point(x, y), mouseHover) || IsSameBlock(new Point(x, y), mouseHover)) &&
							mouseHover != new Point(-1, -1));
					}
				}
			}

			endGame.Draw();
		}


		private bool IsSameBlock(Point p1, Point p2)
		{
			int blockX1 = (int)(p1.X / 3);
			int blockY1 = (int)(p1.Y / 3);
			int blockX2 = (int)(p2.X / 3);
			int blockY2 = (int)(p2.Y / 3);
			return (blockX1 == blockX2 && blockY1 == blockY2);
		}
		private bool IsSameLine(Point p1, Point p2)
		{
			return (p1.X == p2.X || p1.Y == p2.Y);
		}
		public List<int> PossibleValuesFor(int pX, int pY)
		{
			List<int> possible = new List<int>()
			{
				1, 2, 3, 4, 5, 6, 7, 8, 9
			};
			for (int x = 0; x < WIDTH; x++)
			{
				for (int y = 0; y < HEIGHT; y++)
				{
					if (!(x == pX && y == pY) &&
						(IsSameBlock(new Point(x, y), new Point(pX, pY)) || IsSameLine(new Point(x, y), new Point(pX, pY)))
						&& grid[x, y].filled && possible.Contains(grid[x, y].number))
					{
						possible.Remove(grid[x, y].number);
					}
				}
			}

			return possible;
		}
		public bool FillNeeded(bool permanant)
		{
			bool found = false;
			for (int x = 0; x < WIDTH; x++)
			{
				for (int y = 0; y < HEIGHT; y++)
				{
					if (!grid[x, y].filled)
					{
						List<int> possible = PossibleValuesFor(x, y);
						if (possible.Count == 1)
						{
							grid[x, y].filled = true;
							grid[x, y].number = possible[0];
							grid[x, y].permanant = permanant;
							found = true;
							//Console.WriteLine("Filled needed at ({0}, {1})", x, y);
						}
					}
				}
			}

			return found;
		}
	}
}
