﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
	class Program
	{
		public static GameWindow window;
		public static Game game;

		static void Main(string[] args)
		{
			window = new GameWindow(500, 500);
			game = new Game(window);

			window.Run();
		}
	}
}
