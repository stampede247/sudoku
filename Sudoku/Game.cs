﻿using OpenTK;
using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using System.Drawing.Imaging;

namespace Sudoku
{
	class Game
	{
		public GameWindow window;

		public static int border = 20;

		GameBoard board;

		public Game(GameWindow window)
		{
			this.window = window;

			window.Load += window_Load;
			window.RenderFrame += window_RenderFrame;
			window.UpdateFrame += window_UpdateFrame;

			My.Initialize(window);
		}

		void window_Load(object sender, EventArgs e)
		{
			#region Initiate Board
			{
				QFont font = new QFont("Content/Fonts/Default.ttf", 40);
				int max = 0;
				for (int i = 0; i <= 9; i++)
				{
					SizeF size = font.Measure(i.ToString());
					if (size.Width > max)
						max = (int)size.Width;
					if (size.Height > max)
						max = (int)size.Height;
				}

				if (max % 2 != 0)
					max += 1;

				board = new GameBoard(new Rectangle(border, border,
					(max + GameBoard.spacing) * GameBoard.WIDTH + GameBoard.spacing,
					(max + GameBoard.spacing) * GameBoard.HEIGHT + GameBoard.spacing));

				window.ClientSize = new Size(
					(int)(board.gameRec.Width + border * 2),
					(int)(board.gameRec.Height + border * 2 + 100));
			}
			#endregion

		}

		void window_UpdateFrame(object sender, FrameEventArgs e)
		{
			My.Update();

			board.Update(e.Time);

			if (My.KeyPress(Key.R))
			{
				if (board.endGame.running)
				{
					board.endGame.Reset();
				}
				board.CreatePuzzle();
			}

			My.UpdateAfter();
		}

		void window_RenderFrame(object sender, FrameEventArgs e)
		{
			GL.ClearColor(Color.Gray);
			GL.Clear(ClearBufferMask.ColorBufferBit);

			Matrix4 proj = Matrix4.CreateOrthographicOffCenter(0, window.Width, window.Height, 0, 0, 1);
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadMatrix(ref proj);
			GL.Viewport(0, 0, window.ClientSize.Width, window.ClientSize.Height);

			GL.Enable(EnableCap.Texture2D);
			GL.Enable(EnableCap.Blend);
			GL.Disable(EnableCap.DepthTest);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			GL.BindTexture(TextureTarget.Texture2D, board.dot.Id);
			#region Background
			GL.Begin(PrimitiveType.Quads);
			GL.Color3(Color.DarkBlue);
			GL.Vertex2(0, 0);
			GL.Vertex2(window.Width, 0);
			GL.Color3(Color.FromArgb(5, 5, 25));
			GL.Vertex2(window.Width, window.Height);
			GL.Vertex2(0, window.Height);
			GL.End();
			#endregion

			board.Draw();

			GL.Flush();
			window.SwapBuffers();
		}
	}
}
