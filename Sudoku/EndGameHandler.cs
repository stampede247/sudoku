﻿using Box2DX.Collision;
using Box2DX.Common;
using Box2DX.Dynamics;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace Sudoku
{
	class EndGameHandler
	{
		public static float SCALE = 20f;
		private GameBoard board;

		public World world;
		public List<Body> bodies;
		public bool running;

		public EndGameHandler(GameBoard board)
		{
			this.board = board;

			AABB box = new AABB();
			box.LowerBound = new Vec2(0, 0);
			box.UpperBound = new Vec2(Program.window.Width, Program.window.Height);
			world = new World(box, new Vec2(0, 5f), true);
			world.SetContinuousPhysics(false);

			bodies = new List<Body>();
			running = false;
		}
		public void Create()
		{
			running = true;
			CreateStaticWalls();
			CreateDynamicWalls();
			CreateNumbers();
			CreateExplosion();
		}
		public void Reset()
		{
			for (int i = 0; i < bodies.Count; i++)
			{
				world.DestroyBody(bodies[i]);
			}
			bodies = new List<Body>();
			running = false;
		}

		private void CreateStaticWalls()
		{
			BodyDef bd = new BodyDef();
			bd.Position.Set(
				(board.gameRec.Left + board.gameRec.Width / 2f) / SCALE,
				(board.gameRec.Top + board.gameRec.Height / 2f) / SCALE);
			Body body = world.CreateBody(bd);

			PolygonDef sd = new PolygonDef();
			sd.Density = 2f;
			sd.Restitution = 0.4f;
			sd.Friction = 1f;

			sd.SetAsBox((board.gameRec.Width / 2f) / SCALE, 10f / SCALE,
				new Vec2(0, (-board.gameRec.Height / 2f - 10f) / SCALE), 0);
			body.CreateFixture(sd);
			sd.SetAsBox((board.gameRec.Width / 2f) / SCALE, 10f / SCALE,
				new Vec2(0, (board.gameRec.Height / 2f + 10f) / SCALE), 0);
			body.CreateFixture(sd);
			sd.SetAsBox(10f / SCALE, (board.gameRec.Height / 2f + 20f) / SCALE,
				new Vec2((board.gameRec.Width / 2f + 10f) / SCALE, 0f), 0);
			body.CreateFixture(sd);
			sd.SetAsBox(10f / SCALE, (board.gameRec.Height / 2f + 20f) / SCALE,
				new Vec2((-board.gameRec.Width / 2f - 10f) / SCALE, 0f), 0);
			body.CreateFixture(sd);

			bodies.Add(body);
			//ground.SetMassFromShapes();
		}
		private void CreateDynamicBox(RectangleF box, float angle = 0f)
		{
			BodyDef bd = new BodyDef();
			bd.Position.Set((box.Left + box.Width / 2f) / SCALE, (box.Top + box.Height / 2f) / SCALE);
			bd.LinearDamping = 0f;

			Body body = world.CreateBody(bd);

			PolygonDef sd = new PolygonDef();
			sd.Density = 2f;
			sd.Restitution = 0.4f;
			sd.Friction = 1f;

			sd.SetAsBox(box.Width / 2f / SCALE, box.Height / 2f / SCALE, new Vec2(0, 0), angle);
			body.CreateFixture(sd);
			body.SetMassFromShapes();

			bodies.Add(body);
		}
		private void CreateDynamicWalls()
		{
			for (int x = 0; x < GameBoard.WIDTH; x++)
			{
				for (int y = 0; y < GameBoard.HEIGHT; y++)
				{
					RectangleF cellRec = board.GetRec(x, y);
					float thick = ((x + 1) % 3 == 0) ? GameBoard.lineThickness2 : GameBoard.lineThickness1;
					if (x < GameBoard.WIDTH - 1)
					{
						CreateDynamicBox(new RectangleF(
							cellRec.Right + GameBoard.spacing / 2f - thick / 2f,
							cellRec.Top - GameBoard.spacing / 2f,
							thick,
							cellRec.Height + GameBoard.spacing));
					}

					thick = ((y + 1) % 3 == 0) ? GameBoard.lineThickness2 : GameBoard.lineThickness1;
					if (y < GameBoard.HEIGHT - 1)
					{
						CreateDynamicBox(new RectangleF(
							cellRec.Left - GameBoard.spacing / 2f,
							cellRec.Bottom + GameBoard.spacing / 2f - thick / 2f,
							cellRec.Width + GameBoard.spacing,
							thick));
					}
				}
			}
		}
		private void CreateNumberPoly(Vector2 position, int number)
		{
			SizeF size = board.font.Measure(number.ToString());
			CreateDynamicBox(new RectangleF(
				position.X - size.Width / 2f, 
				position.Y - size.Height / 2f,
				size.Width, size.Height));
			bodies[bodies.Count - 1].SetUserData(number);
		}
		private void CreateNumbers()
		{
			for (int x = 0; x < GameBoard.WIDTH; x++)
			{
				for (int y = 0; y < GameBoard.HEIGHT; y++)
				{
					if (board.grid[x, y].filled)
					{
						CreateNumberPoly(new Vector2(
							(x + 0.45f) * (board.tileSize + GameBoard.spacing) + GameBoard.spacing + board.gameRec.X,
							(y + 0.45f) * (board.tileSize + GameBoard.spacing) + GameBoard.spacing + board.gameRec.Y),
							board.grid[x, y].number);
					}
				}
			}
		}
		private void CreateExplosion()
		{
			Vector2 position = new Vector2(Program.window.Mouse.X, Program.window.Mouse.Y) / SCALE;
			for (int i = 0; i < bodies.Count; i++)
			{
				Vector2 bPos = Convert(bodies[i].GetPosition());
				bodies[i].ApplyImpulse( 
					Convert(Vector2.Normalize(bPos - position) * 100f / 
					((bPos - position).Length / 2f)), 
					Vec2.Zero);
				bodies[i].SetAngularVelocity(0);
			}
		}

		public void Update(double time)
		{
			if (running)
			{
				world.Step(1 / 60f, 1, 1);
			}
		}

		public void Draw()
		{
			if (running)
			{
				GL.BindTexture(TextureTarget.Texture2D, board.dot.Id);
				GL.Begin(PrimitiveType.Quads);
				for (int i = 1; i < bodies.Count; i++)
				{
					DrawBody(bodies[i]);
				}
				GL.End();

				QuickFont.QFont.Begin();
				for (int i = 0; i < bodies.Count; i++)
				{
					DrawNumber(bodies[i]);
				}
				QuickFont.QFont.End();
			}
		}

		private void DrawBody(Body b)
		{
			Fixture fixture = b.GetFixtureList();
			while (fixture != null)
			{
				Shape shape = fixture.Shape;
				if (shape != null)
				{
					if (fixture.ShapeType == ShapeType.PolygonShape)
					{
						PolygonShape poly = (PolygonShape)shape;
						if (b.GetUserData() != null && b.GetUserData() is int && poly.VertexCount == 4)
						{
							//XForm form = b.GetXForm();
							//Vector2 p1 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(0)));
							//Vector2 p2 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(1)));
							//Vector2 p3 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(2)));
							//Vector2 p4 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(3)));
							//DrawQuad(p1 * SCALE, p2 * SCALE, p3 * SCALE, p4 * SCALE, System.Drawing.Color.Red);
						}
						else if (poly.VertexCount == 4)
						{
							//OBB boundBox = poly.GetOBB();
							//View.DrawRectangle(Convert(boundBox.Center), Convert(boundBox.Extents) * 2, b.GetAngle(), System.Drawing.Color.Red);
							//Vec2[] verts = poly.GetVertices();
							XForm form = b.GetXForm();
							Vector2 p1 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(0)));
							Vector2 p2 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(1)));
							Vector2 p3 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(2)));
							Vector2 p4 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(3)));
							DrawQuad(p1 * SCALE, p2 * SCALE, p3 * SCALE, p4 * SCALE, System.Drawing.Color.Black);
						}

						if (poly.VertexCount == 3)
						{
							//Vec2[] verts = poly.GetVertices();
							XForm form = b.GetXForm();
							Vector2 p1 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(0)));
							Vector2 p2 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(1)));
							Vector2 p3 = Convert(Box2DX.Common.Math.Mul(form, poly.GetVertex(2)));
							//View.DrawTriangle(p1 * 10, p2 * 10, p3 * 10, System.Drawing.Color.Green);
						}
					}
				}

				fixture = fixture.Next;
			}
		}
		private void DrawNumber(Body b)
		{
			QuickFont.QFontRenderOptions ops = new QuickFont.QFontRenderOptions();
			ops.Colour = System.Drawing.Color.Green;
			board.font.PushOptions(ops);

			Fixture fixture = b.GetFixtureList();
			while (fixture != null)
			{
				Shape shape = fixture.Shape;
				if (shape != null)
				{
					if (fixture.ShapeType == ShapeType.PolygonShape)
					{
						PolygonShape poly = (PolygonShape)shape;
						if (b.GetUserData() != null && b.GetUserData() is int && poly.VertexCount == 4)
						{
							XForm form = b.GetXForm();
							Matrix4 mat4 = Matrix4.CreateTranslation(new Vector3(
								-form.Position.X * SCALE, 
								-form.Position.Y * SCALE,
								0));
							//mat4 *= new Matrix4(
							//	form.R.Col1[0], form.R.Col2[0], 0, 0,
							//	form.R.Col1[1], form.R.Col2[1], 0, 0,
							//	0, 0, 1, 0,
							//	0, 0, 0, 1);
							mat4 *= Matrix4.CreateRotationZ(form.GetAngle());
							mat4 *= Matrix4.CreateTranslation(new Vector3(
								form.Position.X * SCALE,
								form.Position.Y * SCALE,
								0));
							GL.MatrixMode(MatrixMode.Modelview);
							GL.PushMatrix();
							GL.MultMatrix(ref mat4);

							Vector2 position = Convert(form.Position) * SCALE;
							position.X -= board.font.Measure(b.GetUserData().ToString()).Width / 2f;
							position.Y -= board.font.Measure(b.GetUserData().ToString()).Height / 2f;
							board.font.Print(b.GetUserData().ToString(), position);
							GL.PopMatrix();
						}
					}
				}

				fixture = fixture.Next;
			}
		}
		private static Vector2 Convert(Vec2 vec)
		{
			return new Vector2(vec.X, vec.Y);
		}
		private static Vec2 Convert(Vector2 vec)
		{
			return new Vec2(vec.X, vec.Y);
		}
		private static Vector2[] Convert(Vec2[] vecs)
		{
			Vector2[] grid = new Vector2[vecs.Length];
			for (int i = 0; i < vecs.Length; i++)
			{
				grid[i] = Convert(vecs[i]);
			}
			return grid;
		}
		private void DrawQuad(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, System.Drawing.Color color)
		{
			GL.Color4(color);
			GL.TexCoord2(0, 0); GL.Vertex2(p1);
			GL.TexCoord2(1, 0); GL.Vertex2(p2);
			GL.TexCoord2(1, 1); GL.Vertex2(p3);
			GL.TexCoord2(0, 1); GL.Vertex2(p4);
		}
	}
}
