﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;

namespace Sudoku
{
    struct Texture2D
    {
        /// <summary>
        /// This number is in milliseconds since start of game
        /// </summary>
        private static double Timer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newTime">Milliseconds since start of game</param>
        public static void UpdateTimer(double newTime)
        {
            Timer = newTime;
        }

        private int id;
        private Vector2 size;
        private string fileName;
        private bool isAnimation;
        private int numFrames;
        private float milsPerFrame;

        public string FileName
        {
            get { return fileName; }
        }
        public bool IsAnimation
        {
            get
            {
                return isAnimation;
            }
        }
        public int NumFrames
        {
            get
            {
                return numFrames;
            }
        }
        public Vector2 Size
        {
            get 
            { 
                return new Vector2(size.X, size.Y); 
            }
        }
        public int Id
        {
            get { return id; }
        }
        public int Width
        {
            get
            {
                return (int)size.X;
            }
        }
        public int Height
        {
            get
            {
                return (int)size.Y;
            }
        }
        public Vector2 CellSize
        {
            get
            {
                return new Vector2(size.X / numFrames, size.Y);
            }
        }
        public float CellWidth
        {
            get
            {
                return size.X / numFrames;
            }
        }
        public float CellHeight
        {
            get
            {
                return size.Y;
            }
        }
        public float MilisecondsPerFrame
        {
            get
            {
                return milsPerFrame;
            }
            set
            {
                this.milsPerFrame = value;
            }
        }
        public float FramesPerSecond
        {
            get
            {
                return 1000f / milsPerFrame;
            }
            set
            {
                this.milsPerFrame = 1000f / value;
            }
        }
        public int CurrentFrame
        {
            get
            {
                return (int)(Timer / milsPerFrame) % numFrames;
            }
        }
        public RectangleF CurrentCellRec
        {
            get
            {
                return new RectangleF(CurrentFrame * CellWidth, 0, CellWidth, CellHeight);
            }
        }
		public bool IsNull
		{
			get { return (this.size == Vector2.Zero); }
		}

        public Texture2D(string fileName, int id, int width, int height)
        {
            this.fileName = fileName;
            this.id = id;
            this.size = new Vector2(width, height);
            this.isAnimation = false;
            this.numFrames = 1;
            this.milsPerFrame = 1;
        }
        public Texture2D(string fileName, int id, int stripWidth, int stripHeight, int numFrames, float milsPerFrame = 1000f / 20f)
        {
            this.fileName = fileName;
            this.id = id;
            this.size = new Vector2(stripWidth, stripHeight);
            this.isAnimation = true;
            this.numFrames = numFrames;
            this.milsPerFrame = milsPerFrame;
        }
    }
}
