﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using System.IO;

namespace Sudoku
{
    class ContentPipe
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">Defaults to Content/Sprites/</param>
		/// <param name="pixelated"></param>
		/// <param name="sourceRec"></param>
		/// <returns></returns>
        public static Texture2D LoadTexture(string filename, bool pixelated = true)
        {
            string filePath = "Content/Sprites/" + filename;
            if (filePath == "")
            {
                throw new ArgumentException(filePath);
                return new Texture2D();
            }

            if (!System.IO.File.Exists(filePath))
            {
                throw new ArgumentException(filePath);
                return new Texture2D();
            }

            bool isAnimated = false;
            int numFrames = 1;
            #region Check if animation strip
            {
                string fileName = Path.GetFileNameWithoutExtension(filePath);
                string folder = Path.GetDirectoryName(filePath);
                string infoFilePath = folder + "/" + fileName + "Info.ani";
                if (File.Exists(infoFilePath))
                {
                    using (StreamReader reader = new StreamReader(infoFilePath))
                    {
                        if (!int.TryParse(reader.ReadLine(), out numFrames))
                        {
                            numFrames = 1;
                            isAnimated = false;
                        }
                        else
                        {
                            isAnimated = true;
                        }
                    }
                }
            }
            #endregion


            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);

            Bitmap bmp = new Bitmap(filePath);
			BitmapData bmpData;
			bmpData = bmp.LockBits(
				new Rectangle(0, 0, bmp.Width, bmp.Height),
				ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
				bmpData.Width, 
				bmpData.Height, 
				0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                PixelType.UnsignedByte, bmpData.Scan0);

            Texture2D tex;
            if (!isAnimated)
            {
				tex = new Texture2D(filename, id, bmpData.Width, bmpData.Height);
            }
            else
            {
				tex = new Texture2D(filename, id, bmpData.Width, bmpData.Height, numFrames);
            }

            bmp.UnlockBits(bmpData);

            //We haven't unloaded mipmaps, so disavle mipmapping (otherwise the texture will not appear).
            //On newer video cards, we can use GL.GenerateMipmaps() of GL.Ext.GenerateMipmaps() to create
            //mipmaps automatically. In that case, use TextureMinFilter.LinearMipmapLinear to enable them.
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, pixelated ? (int)TextureMinFilter.Nearest : (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, pixelated ? (int)TextureMinFilter.Nearest : (int)TextureMinFilter.Linear);

            return tex;
        }
    }
}
