﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
	/// <summary>
	/// This class automatically returns a previously loaded texture if it has been loaded in 
	/// or loads it in for the first time and saves it for later too
	/// </summary>
	class TextureHandler
	{
		private static Dictionary<string, Texture2D> textures;

		/// <summary>
		/// Automatically returns a previously loaded texture if it has been loaded in 
		/// or loads it in for the first time and saves it for later too
		/// </summary>
		/// <param name="fileName">Defaults inside "Content/"</param>
		/// <returns>Either a freshly loaded texture or previously loaded one with same path</returns>
		public static Texture2D GetTexture(string fileName)
		{
			if (textures == null)
				textures = new Dictionary<string, Texture2D>();

			if (textures.ContainsKey(fileName))
			{
				return textures[fileName];
			}
			else
			{
				textures.Add(fileName, ContentPipe.LoadTexture(fileName));
				return textures[fileName];
			}
		}
	}
}
