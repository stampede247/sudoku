﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickFont;

namespace Sudoku
{
	class GridSpace
	{
		public int x, y;
		public bool filled;
		public bool permanant;
		public int number;

		public GridSpace(int x, int y, bool permanant = false)
		{
			this.x = x;
			this.y = y;
			this.number = 0;
			this.filled = false;
			this.permanant = permanant;
		}

		public void Draw(GameBoard board, Rectangle rec, bool hovering, bool aligned)
		{
			GL.BindTexture(TextureTarget.Texture2D, board.dot.Id);
			GL.Begin(PrimitiveType.Quads);

			#region Highlighting
			if (hovering && !this.permanant)
			{
				rec.Inflate((int)(GameBoard.spacing / 2), (int)(GameBoard.spacing / 2));
				GL.Color4(Color.FromArgb(200, Color.DarkBlue));
				GL.TexCoord2(0, 0); GL.Vertex2(rec.Left, rec.Top);
				GL.TexCoord2(1, 0); GL.Vertex2(rec.Right, rec.Top);
				GL.TexCoord2(1, 1); GL.Vertex2(rec.Right, rec.Bottom);
				GL.TexCoord2(0, 1); GL.Vertex2(rec.Left, rec.Bottom);
				rec.Inflate((int)(-GameBoard.spacing / 2), (int)(-GameBoard.spacing / 2));
			}
			else if (aligned)
			{
				rec.Inflate((int)(GameBoard.spacing / 2), (int)(GameBoard.spacing / 2));
				GL.Color4(Color.FromArgb(40, Color.DarkBlue));
				GL.TexCoord2(0, 0); GL.Vertex2(rec.Left, rec.Top);
				GL.TexCoord2(1, 0); GL.Vertex2(rec.Right, rec.Top);
				GL.TexCoord2(1, 1); GL.Vertex2(rec.Right, rec.Bottom);
				GL.TexCoord2(0, 1); GL.Vertex2(rec.Left, rec.Bottom);
				rec.Inflate((int)(-GameBoard.spacing / 2), (int)(-GameBoard.spacing / 2));
			}
			#endregion

			#region Grid Spaces
			for (int x = 0; x < GameBoard.WIDTH; x++)
			{
				for (int y = 0; y < GameBoard.HEIGHT; y++)
				{
					//GL.Color4(Color.FromArgb(grid[x, y].permanant ? 100 : 130, Color.Black));
					//GL.TexCoord2(0, 0); GL.Vertex2(rec.Left, rec.Top);
					//GL.TexCoord2(1, 0); GL.Vertex2(rec.Right, rec.Top);
					//GL.TexCoord2(1, 1); GL.Vertex2(rec.Right, rec.Bottom);
					//GL.TexCoord2(0, 1); GL.Vertex2(rec.Left, rec.Bottom);

					if (!this.filled &&
						hovering &&
						board.mouseInsideHover != new Point(-1, -1))
					{
						GL.Color4(Color.FromArgb(255, Color.Blue));
						Rectangle tempRec = rec;
						tempRec.Width = rec.Width / 3;
						tempRec.Height = rec.Height / 3;
						tempRec.X = tempRec.X + tempRec.Width * board.mouseInsideHover.X;
						tempRec.Y = tempRec.Y + tempRec.Height * board.mouseInsideHover.Y;
						GL.TexCoord2(0, 0); GL.Vertex2(tempRec.Left, tempRec.Top);
						GL.TexCoord2(1, 0); GL.Vertex2(tempRec.Right, tempRec.Top);
						GL.TexCoord2(1, 1); GL.Vertex2(tempRec.Right, tempRec.Bottom);
						GL.TexCoord2(0, 1); GL.Vertex2(tempRec.Left, tempRec.Bottom);
					}
				}
			}
			#endregion

			GL.End();

			QFont.Begin();
			#region Draw Number
			if (this.filled)
			{
				Vector2 center = new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f);
				center.Y -= board.font.Measure(this.number.ToString()).Height / 2f;

				QFontRenderOptions ops = new QFontRenderOptions();
				ops.Colour = this.permanant ? Color.FromArgb(5, 5, 25) : Color.Blue;
				if (board.completed[this.number])
					ops.Colour = Color.Green;
				if (board.mouseHover != new Point(-1, -1) &&
					board.grid[board.mouseHover.X, board.mouseHover.Y].filled &&
					board.grid[board.mouseHover.X, board.mouseHover.Y].number == this.number)
					ops.Colour = this.permanant ? Color.OrangeRed : Color.Orange;
				if (this.filled && !board.PossibleValuesFor(x, y).Contains(this.number))
					ops.Colour = Color.Red;
				board.font.PushOptions(ops);

				board.font.Print(this.number.ToString(), QFontAlignment.Centre, center);
			}
			else if (hovering)
			{
				int i = 0;
				for (int y2 = 0; y2 < 3; y2++)
				{
					for (int x2 = 0; x2 < 3; x2++)
					{
						i++;
						Vector2 pos = new Vector2(
							rec.X + rec.Width / 3f * (x2 + 0.5f),
							rec.Y + rec.Height / 3f * (y2 + 0.5f));
						pos.Y -= board.fontSmall.Measure(i.ToString()).Height / 2f;

						QFontRenderOptions ops = new QFontRenderOptions();
						ops.Colour = Color.Gray;
						if (board.mouseInsideHover != new Point(-1, -1) && new Point(x2, y2) == board.mouseInsideHover)
							ops.Colour = Color.White;
						board.fontSmall.PushOptions(ops);

						board.fontSmall.Print(i.ToString(), QFontAlignment.Centre, pos);
					}
				}
			}
			#endregion
			QFont.End();
		}
	}
}
