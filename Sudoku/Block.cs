﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
	class Block
	{
		private GameBoard board;
		public int blockX, blockY;
		public GridSpace[,] grid;

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Local point values, not global grid coordinates</returns>
		public Point[] FreeSpaces()
		{
			List<Point> free = new List<Point>();
			for (int x = 0; x < 3; x++)
			{
				for (int y = 0; y < 3; y++)
				{
					if (!grid[x, y].filled)
					{
						free.Add(new Point(x, y));
					}
				}
			}

			return free.ToArray();
		}
		public int[] FilledNumbers()
		{
			List<int> nums = new List<int>();

			for (int x = 0; x < 3; x++)
			{
				for (int y = 0; y < 3; y++)
				{
					if (grid[x, y].filled && !nums.Contains(grid[x,y].number))
					{
						nums.Add(grid[x, y].number);
					}
				}
			}

			return nums.ToArray();
		}
		public int[] FreeNumbers()
		{
			List<int> nums = new List<int>()
			{
				1, 2, 3, 4, 5, 6, 7, 8, 9
			};

			for (int x = 0; x < 3; x++)
			{
				for (int y = 0; y < 3; y++)
				{
					if (grid[x, y].filled && nums.Contains(grid[x, y].number))
					{
						nums.Remove(grid[x, y].number);
					}
				}
			}

			return nums.ToArray();
		}

		public Block(int blockX, int blockY, GameBoard board)
		{
			this.blockX = blockX;
			this.blockY = blockY;
			this.board = board;

			grid = new GridSpace[3, 3];
			for (int x = blockX * 3; x < blockX * 3 + 3; x++)
			{
				for (int y = blockY * 3; y < blockY * 3 + 3; y++)
				{
					grid[x - blockX * 3, y - blockY * 3] = board.grid[x, y];
				}
			}
		}
	}
}
